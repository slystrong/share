from django.urls import path
from resources import views


urlpatterns = [
    path('index/', views.index, name='index'),
]